'use strict';
const MAIN_BLOCK = 'group-list';
const GROUP_HEADER_ELEMENT = 'group-list__header';
const ITEM_ELEMENT = 'group-list__item';
const ITEM_LAST_WORD = 'group-list__item-last-word';


class GroupList {
    constructor(selector, options = {}, data = []) {
        this.setDefaults();
        this.select = document.querySelector(selector);
        this._initOptions(options);
        this._initData(data);
        this._initElements();
    }

    setDefaults() {
        this.curYPos = 0;
        this.curDown = false;

        this.itemText = function (item) {
            let ret = Object.values(item);
            let lastWord = ret.pop();
            ret.push(`<span class="${ITEM_LAST_WORD}">${lastWord}</span>`);
            return ret.join(" ");
        };

        this.headerText = function (item) {
            return item['header'];
        };
        this.groupBy = null;
    }

    _initOptions(options) {
        for (let key in options) {
            if (options.hasOwnProperty(key)) {
                this[key] = options[key];
            }
        }
    }

    _groupData(data) {
        if (data.length) {
            if (!this.groupBy) {
                this.groupBy = Object.keys(data[0])[0];
            }
            const sorted = data.sort((a, b) => a[this.groupBy].localeCompare(b[this.groupBy]));
            return sorted.reduce((final, item) => {
                const firstChar = item[this.groupBy][0];
                if (final.length === 0) addHeader();
                else {
                    const lastItemFirstChar = final[final.length - 1][this.groupBy][0];
                    if (lastItemFirstChar !== firstChar) addHeader();
                }
                final.push(item);
                return final;

                function addHeader() {
                    final.push({header: firstChar});
                }
            }, []);
        }
    }

    _createListHeader(item) {
        let listItem = document.createElement('div');
        listItem.classList.add(GROUP_HEADER_ELEMENT);
        listItem.innerHTML = this.headerText(item);
        return listItem;
    }

    _createListItem(item) {
        let listItem = document.createElement('div');
        listItem.classList.add(ITEM_ELEMENT);
        listItem.innerHTML = this.itemText(item);
        return listItem;
    }

    _initElements() {
        this.groupList = document.createElement('div');
        this.groupList.classList.add(MAIN_BLOCK);
        this._initMouseMove();
        this._initMouseDown();
        this._initMouseUp();
        this.groupList.addEventListener('scroll', this._scrolling.bind(this));
    }

    _initData(data) {
        this.data = this._groupData(data);
    }

    _initMouseMove() {
        this.groupList.addEventListener('mousemove', function (e) {
            if (this.curDown === true) {
                this.scrollTop += this.curYPos - (e.clientY + this.scrollTop);
            }
        });
    }

    _scrolling() {
        for (let i = 0; i < this.headers.length; i++) {
            let thisSticky = this.headers[i];
            let stickyPosition = thisSticky.getAttribute('originalPosition');

            if (stickyPosition <= this.groupList.scrollTop) {
                if (i < this.headers.length - 1) {
                    let nextSticky = this.headers[i + 1];
                    let nextStickyPosition = nextSticky.getAttribute('originalPosition') - thisSticky.getAttribute('originalHeight');
                    if (this.groupList.scrollTop <= nextStickyPosition) {
                        thisSticky.style.top = (this.groupList.scrollTop - stickyPosition) + 'px';
                    } else {
                        thisSticky.style.top = (nextStickyPosition - stickyPosition) + 'px';
                    }
                }
            } else {
                thisSticky.style.top = 'auto';
            }
        }
    };

    _initMouseUp() {
        this.groupList.addEventListener('mouseup', function (e) {
            this.curDown = false;
        });
    }

    _initMouseDown() {
        this.groupList.addEventListener('mousedown', function (e) {
            this.curDown = true;
            this.curYPos = e.clientY + this.scrollTop;
        });
    }

    redraw() {
        let container = document.createElement('div');
        for (let i = 0; i < this.data.length; i++) {
            let item = this.data[i];
            if ('header' in item) {
                if (i === 0) {
                    this.groupList.appendChild(this._createListHeader(item));
                } else {
                    this.groupList.appendChild(container);
                    container = document.createElement('div');
                    this.groupList.appendChild(this._createListHeader(item));
                }
            } else {
                container.appendChild(this._createListItem(item));
            }
        }
        this.groupList.appendChild(container);
        this.select.appendChild(this.groupList);
        this.groupList.style.width = this.select.offsetWidth + 'px';
        this.headers = this.groupList.getElementsByClassName(GROUP_HEADER_ELEMENT);
        for (let i = 0; i < this.headers.length; i++) {
            this.headers[i].setAttribute('originalPosition', this.headers[i].offsetTop);
            this.headers[i].setAttribute('originalHeight', this.headers[i].offsetHeight);
        }
    }

}

export {GroupList};
